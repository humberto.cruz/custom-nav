import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { View } from 'react-native';
import Navigation from './Navigation'
import { Text, Provider, DataTable, Caption, Title, Appbar } from 'react-native-paper'
import { SafeAreaView, SafeAreaProvider } from 'react-native-safe-area-context'
import AppNavigator from './Navigation';

export default function App() {
  return (
    <SafeAreaProvider>
    <SafeAreaView style={{flex:1}}>
        <Appbar.Header>
          <Appbar.Content title={'Hello'} />
        </Appbar.Header>
      <View style={{backgroundColor:'#69C',flex:8}}>
        <Navigation />
      </View>
    </SafeAreaView>
    </SafeAreaProvider>
  )
}
