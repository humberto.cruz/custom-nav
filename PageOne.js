import React from 'react'
import { Text, View } from 'react-native'
import { Button, Paragraph } from 'react-native-paper'

const PageOne = (props) => {
    return(
        <View style={{paddingHorizontal:10}}>
            <Paragraph>Page One</Paragraph>
            <Button onPress={()=>props.navigation.navigate('PageTwo')} mode={'contained'} >Go To Page Two</Button>
        </View>
    )
}

export default PageOne
