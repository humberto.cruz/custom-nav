import React from 'react'
import { Text, View } from 'react-native'
import { Paragraph, Button  } from 'react-native-paper'

const PageOne = (props) => {
    return(
        <View style={{paddingHorizontal:10}}>
            <Paragraph>Page Two</Paragraph>
            <Button onPress={()=>props.navigation.navigate('PageOne')} mode={'outlined'} >Go To Page One</Button>
        </View>
    )
}

export default PageOne
